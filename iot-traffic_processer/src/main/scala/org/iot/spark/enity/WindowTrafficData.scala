package org.iot.spark.enity

import java.util.Date

import com.fasterxml.jackson.annotation.JsonFormat

case class WindowTrafficData (
  routeId:String,
  vehicleType:String,
  totalCount:Long,
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "MST")
  timeStamp: Date,
  recordDate:String
)
